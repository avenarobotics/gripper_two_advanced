/*
 * FeeTechRS485.c
 *
 *  Created on: 29 mar 2021
 *      Author: John
 */

#include "FeeTechRS485.h"
#include "main.h"


extern	UART_HandleTypeDef huart2;

extern int16_t Servo_Position[2];
extern uint8_t Servo_Temp[2];
extern uint8_t Servo_Status[2];

uint8_t RS485_RX[8] = {0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
uint8_t FrameType = 0;



int MovePosition(unsigned char ID, signed short int position, unsigned short int velocity)
{
	if (FrameType == TransferComplete)
	{
		FrameType = SendMove;

		uint8_t CRC_ID = 0;
		uint8_t CRC_SUM = 0;
		uint8_t CRC_SUM_temp = 0;

		uint8_t size = 13;
		uint8_t RS485_TX[13] = {0xFF,0xFF}; // Transmitted data buffer, Inital frame data


		RS485_TX[2] = ID;
		RS485_TX[3] = 0x09; // DATA LENGTH
		RS485_TX[4] = 0x03; // INSTRUCTION - WRITE DATA
// Parameters
		RS485_TX[5] = 0x2A;
		RS485_TX[6] = position;
		RS485_TX[7] = position >> 8;
		RS485_TX[8] = 0x00;
		RS485_TX[9] = 0x00;
		RS485_TX[10] = velocity;
		RS485_TX[11] = velocity >> 8 ;

// CRC Calculation
		CRC_SUM_temp = 0;
		for (int i =2; i <= RS485_TX[3]+2; i++)
		{
			CRC_SUM_temp = CRC_SUM_temp + RS485_TX[i];
		}
		CRC_SUM = 0xFF - CRC_SUM_temp;
// CRC include in frame
		CRC_ID = size - 1;
		RS485_TX[CRC_ID] = CRC_SUM;

// Transmission
		HAL_UART_Transmit_IT(&huart2, RS485_TX, size);
		while (__HAL_UART_GET_FLAG(&huart2, UART_FLAG_TC) == RESET)
		{
		}
		HAL_UART_Receive_IT(&huart2, RS485_RX, 6);

		return ID;
	}
	else
	{
		return 0xFF;
	}
}


int Ping(unsigned char ID)
{
	if (FrameType == TransferComplete)
	{
		FrameType = SendPing;

		uint8_t CRC_ID = 0;
		uint8_t CRC_SUM = 0;
		uint8_t CRC_SUM_temp = 0;

		uint8_t size = 6;
		uint8_t RS485_TX[6] = {0xFF,0xFF}; // Transmitted data buffer, Inital frame data


		RS485_TX[2] = ID;
		RS485_TX[3] = 0x02; // DATA LENGTH
		RS485_TX[4] = 0x01; // INSTRUCTION - PING

 // CRC Calculation
		CRC_SUM_temp = 0;
		for (int i =2; i <= RS485_TX[3]+2; i++)
		{
			CRC_SUM_temp = CRC_SUM_temp + RS485_TX[i];
		}
		CRC_SUM = 0xFF - CRC_SUM_temp;
// CRC include in frame
		CRC_ID = size - 1;
		RS485_TX[CRC_ID] = CRC_SUM;

// Transmission
		HAL_UART_Transmit_IT(&huart2, RS485_TX, size);
		while (__HAL_UART_GET_FLAG(&huart2, UART_FLAG_TC) == RESET)
		{
		}
		HAL_UART_Receive_IT(&huart2, RS485_RX, size);
		return ID;
	}
	else
	{
		return 0xFF;
	}
}
	//	Verify incoming frame


int AskPosition(unsigned char ID)
{


	// 	UART - RS485 COMMUNICATION
	if (FrameType == TransferComplete)
	{
		FrameType = SendPosition;
		uint8_t CRC_ID = 0;
		uint8_t CRC_SUM = 0;
		uint8_t CRC_SUM_temp = 0;

		uint8_t size = 8;
		uint8_t RS485_TX[8] = {0xFF,0xFF}; // Transmitted data buffer, Inital frame data


		RS485_TX[2] = ID;
		RS485_TX[3] = 0x04; // DATA LENGTH
		RS485_TX[4] = 0x02; // INSTRUCTION - READ DATA
// Parameters
		RS485_TX[5] = 0x38; // REGISTER ADDRES
		RS485_TX[6] = 0x02; // NUMBER OF BITS TO READ

// CRC Calculation
		CRC_SUM_temp = 0;
		for (int i =2; i <= RS485_TX[3]+2; i++)
		{
			CRC_SUM_temp = CRC_SUM_temp + RS485_TX[i];
		}
		CRC_SUM = 0xFF - CRC_SUM_temp;
// CRC include in frame
		CRC_ID = size - 1;
		RS485_TX[CRC_ID] = CRC_SUM;

// Transmission
		HAL_UART_Transmit_IT(&huart2, RS485_TX, size);
		while (__HAL_UART_GET_FLAG(&huart2, UART_FLAG_TC) == RESET)
		{
		}
		HAL_UART_Receive_IT(&huart2, RS485_RX, size);
		return ID;
	}
	else
	{
		return 0xFF;
	}
}



int AskTemp(unsigned char ID)
{
	if (FrameType == TransferComplete)
	{
		FrameType = SendTemp;
		uint8_t CRC_ID = 0;
		uint8_t CRC_SUM = 0;
		uint8_t CRC_SUM_temp = 0;

		uint8_t size = 8;
		uint8_t RS485_TX[8] = {0xFF,0xFF}; // Transmitted data buffer, Inital frame data


		RS485_TX[2] = ID;
		RS485_TX[3] = 0x04; // DATA LENGTH
		RS485_TX[4] = 0x02; // INSTRUCTION - READ DATA
// Parameters
		RS485_TX[5] = 0x3F; // REGISTER ADDRES
		RS485_TX[6] = 0x01; // NUMBER OF REGISTERS TO READ

// CRC Calculation
		CRC_SUM_temp = 0;
		for (int i =2; i <= RS485_TX[3]+2; i++)
		{
			CRC_SUM_temp = CRC_SUM_temp + RS485_TX[i];
		}
		CRC_SUM = 0xFF - CRC_SUM_temp;
// CRC include in frame
		CRC_ID = size - 1;
		RS485_TX[CRC_ID] = CRC_SUM;

// Transmission
		HAL_UART_Transmit_IT(&huart2, RS485_TX, size);
		while (__HAL_UART_GET_FLAG(&huart2, UART_FLAG_TC) == RESET)
		{
		}
		HAL_UART_Receive_IT(&huart2, RS485_RX, 7);
		return ID;
	}
	else
	{
		return 0xFF;
	}
}

void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{
	uint8_t CRC_ID = 0;
	uint8_t CRC_SUM = 0;
	uint8_t CRC_SUM_temp = 0;
	uint8_t ID = 0;


	CRC_SUM_temp = 0;
	CRC_ID = RS485_RX[3]+3;
	for (int i =2; i <= RS485_RX[3]+2; i++)
	{
		CRC_SUM_temp = CRC_SUM_temp + RS485_RX[i];
	}
	CRC_SUM = 0xFF - CRC_SUM_temp;
	if (CRC_SUM == RS485_RX[CRC_ID]) // CRC OK!
	{

		ID = RS485_RX[2];
		if (FrameType == SendPing)
		{
			Servo_Status[ID] = 0xFF - RS485_RX[4];
		}
		if (FrameType == SendMove)
		{
			Servo_Status[ID] = 0xFF - RS485_RX[4];
		}
		if (FrameType == SendPosition)
		{
			Servo_Status[ID] = 0xFF - RS485_RX[4];
			Servo_Position[ID] = RS485_RX[6] << 8;
			Servo_Position[ID] = Servo_Position[ID] + RS485_RX[5];
		}
		if (FrameType == SendTemp)
		{
			Servo_Status[ID] = 0xFF - RS485_RX[4];
			Servo_Temp[ID] = RS485_RX[5];
		}

	}
	else
	{

	}
	FrameType = TransferComplete;
}









